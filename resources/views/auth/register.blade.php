@extends('layouts.app')

@section('content')

<div class="col-sm-4 col-sm-offset-4">
    <div class="panel panel-primary">
        <div class="panel-heading">Register</div>
        <div class="panel-body">
            <form action="{{ route('register') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label class="control-label">
                        @if ($errors->has('name'))
                            {{ $errors->first('name') }}
                        @else
                            Name
                        @endif
                    </label>
                    <input type="text" class="form-control" name="name"  required autofocus>
                </div>

                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label">
                        @if ($errors->has('email'))
                            {{ $errors->first('email') }}
                        @else
                            Email
                        @endif
                    </label>
                    <input type="email" class="form-control" name="email" required>
                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label">
                        @if ($errors->has('password'))
                            {{ $errors->first('password') }}
                        @else
                            Password
                        @endif
                    </label>
                    <input type="password" class="form-control" name="password" required>
                </div>

                <div class="form-group">
                    <label class="control-label">
                        @if ($errors->has('password_confirmation'))
                            {{ $errors->first('password_confirmation') }}
                        @else
                            Confirm Password
                        @endif
                    </label>
                    <input type="password" class="form-control" name="password_confirmation" required>
                </div>

                <button type="submit" class="btn btn-success btn-block">
                    Register
                </button>
            </form>
        </div>
    </div>
</div>

@endsection
