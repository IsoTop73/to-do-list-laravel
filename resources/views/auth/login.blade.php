@extends('layouts.app')

@section('content')

<div class="col-sm-4 col-sm-offset-4">
    <div class="panel panel-primary">
        <div class="panel-heading">Login</div>
        <div class="panel-body">
            <form action="{{ route('login') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="control-label">
                        @if ($errors->has('email'))
                            {{ $errors->first('email') }}
                        @else
                            Email
                        @endif
                    </label>
                    <input type="email" class="form-control" name="email" autofocus>
                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label">
                        @if ($errors->has('password'))
                            {{ $errors->first('password') }}
                        @else
                            Password
                        @endif
                    </label>
                    <input type="password" class="form-control" name="password">
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                </div>

                <button type="submit" class="btn btn-success btn-block">Login</button>
            </form>
        </div>
    </div>
</div>

@endsection
