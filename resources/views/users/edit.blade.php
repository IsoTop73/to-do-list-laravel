@extends('layouts.app')

@section('content')

<div class="col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">Profile</div>
        <div class="panel-body">
            <form action="{{url('/profile')}}" method="POST" class="profile-form">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label {{ $errors->has('name') ? 'has-error' : '' }}">
                        @if ($errors->has('name'))
                            {{ $errors->first('name') }}
                        @else
                            Name
                        @endif
                    </label>
                    <input type="text" class="form-control" value="{{$user->name}}"
                    placeholder="Name" name="name">
                </div>
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="control-label">
                        @if ($errors->has('email'))
                            {{ $errors->first('email') }}
                        @else
                            Email
                        @endif
                    </label>
                    <input type="email" class="form-control" value="{{$user->email}}"
                    placeholder="Email" name="email">
                </div>
                <button type="submit" class="btn btn-success btn-block">Save</button>
            </form>
        </div>
    </div>
</div>

@endsection