@extends('layouts.app')

@section('content')
<div class="col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">To do</div>
        <div class="panel-body">

            @if (count($tasks) > 0)

                <ul class="list-group">

                    @foreach($tasks as $task)

                        <li class='list-group-item clearfix'>
                            <strong>
                                {{ $task->name }}
                            </strong>
                            <div class="controls pull-right">
                                <a href="/tasks/{{$task->id}}/edit" class="btn btn-primary btn-xs">
                                    Edit
                                </a>
                                <button class="btn btn-danger btn-xs"
                                    form="delete-task-form-{{$task->id}}">
                                    Delete
                                </button>
                            </div>
                            <form action="/tasks/{{$task->id}}"
                                method="POST"
                                id="delete-task-form-{{$task->id}}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        </li>

                    @endforeach

                </ul>
                <hr>

            @endif

            <form method="POST" action="{{url('tasks')}}">
                {{ csrf_field() }}

                <label class="control-label">
                    New task
                </label>
                <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">

                    @if ($errors->has('name'))

                        <label class="control-label">
                            {{ $errors->first('name') }}
                        </label>

                    @endif

                    <input name="name" type="text"
                        class="form-control"
                        placeholder="Name"
                        autofocus>
                </div>

                <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">

                    @if ($errors->has('description'))

                        <label class="control-label">
                            {{ $errors->first('description') }}
                        </label>

                    @endif

                    <textarea class="form-control"
                        name="description"
                        placeholder="Description"
                        rows="3"></textarea>
                </div>

                <button type="submit" class="btn btn-success">
                    Add item
                </button>
            </form>
        </div>
    </div>
</div>

@endsection