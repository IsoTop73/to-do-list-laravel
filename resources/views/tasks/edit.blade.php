@extends('layouts.app')

@section('content')

<div class="col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">Edit</div>
        <div class="panel-body">
            <form id="item-form" action='{{url("/tasks/$task->id")}}' method="POST">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label class="control-label">
                        @if ($errors->has('name'))
                            {{ $errors->first('name') }}
                        @else
                            Name
                        @endif
                    </label>
                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{$task->name}}">
                </div>

                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label class="control-label">
                        @if ($errors->has('description'))
                            {{ $errors->first('description') }}
                        @else
                            Name
                        @endif
                    </label>
                    <div class="form-group">
                        <textarea class="form-control" name="description" placeholder="Description" rows="5">{{$task->description}}</textarea>
                    </div>
                </div>
                
                <button type="submit" class="btn btn-success btn-block">Save</button>
            </form>
        </div>
    </div>
</div>

@endsection