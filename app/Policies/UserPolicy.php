<?php

namespace App\Policies;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function edit(Request $request, User $user)
    {
        return $request->user->id === $user->id || $request->user->admin;
        // return true;
    }
}
